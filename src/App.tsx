import React, { useState } from 'react';
import { TodoList } from './TodoList';
import { AddTodo } from './AddTodo';


const intitalTodos: Array<Todo> = [{
  text: "Task ",
  complete: true
},
{
  text: "Task 2 ",
  complete: false
}]

const App: React.FC = () => {
  const [todos, setTodos] = useState(intitalTodos)
  const toggleTodo: toggleTodo = (selectedTodo) => {
    const newTodos = todos.map(todo => {
      if (todo === selectedTodo) {
        return {
          ...todo,
          complete: !todo.complete
        }
      }
      return todo
    })
    setTodos(newTodos)
  }

  const addTodo: addTodo = (newTodo) => {
    newTodo.trim() !== "" && setTodos([...todos, { text: newTodo, complete: false }])
  }

  return (
    <React.Fragment>
      <TodoList todos={todos} toggleTodo={toggleTodo} />
      <AddTodo addTodo={addTodo} />
    </React.Fragment>)
}

export default App;
